#ushnai-costdb
FROM python:3.7.6-slim

COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY db_connector.py /
COPY process.py /
COPY id_rsa /
CMD python  /process.py