import re
import time
import pymongo
import pandas as pd
import numpy as np
from pandas import DataFrame
from multiprocessing import Pool
from db_connector import connect_to_db

start=time.time()
server=connect_to_db()
server.start()

# server.local_bind_port is assigned local port
client = pymongo.MongoClient('127.0.0.1', server.local_bind_port)

'''connect to WENN database'''
db=client['WENN']
print(db.collection_names())
df=DataFrame(list(db.part_inference_batch_002.find()))
df=df.loc[df['score']>= 0.5]
print('df end')
'''extract case_ids from image names'''
case_id_list = []
for i in df.image_key:
  im_name = i
  case_id = re.split('/|_', im_name)[4]
  case_id_list.append(case_id)
  # damage_percentage=df.damages[i]
  # damage_percentage=[dp[1] for dp in damage_percentage]
  # if len(damage_percentage)!=0:
  #    df['total_damage_perceantage'][i]=(sum(damage_percentage)/len(damage_percentage))
  # else:
  #    df['total_damage_perceantage'][i]=0

df['case_id'] = case_id_list
print("finished")

print(df.to_string())

# drop unnecessary columns
df=df.drop(['_id','xmin','ymin','xmax','ymax','score','damages'],axis=1)

# '''create new database'''
costDB=client["cost_db"]
collection=costDB["batch_002_filtered_0.5"]

unique_cases = df.case_id.unique()

for case in unique_cases:
   df1=df.loc[df['case_id']==case]
   unique_parts=df1.class_name.unique()
   newdf=DataFrame(columns=['part_name', 'damage_percentage'], index=range(len(unique_parts)))

   for i, class_name in enumerate(unique_parts):
       mask = df1['class_name'].isin([class_name])
       df2 = df1[mask]
       # print(df2.to_string())
       if class_name=='31':
           newdf['part_name'][i] = 'unknown_31'
       else:
           newdf['part_name'][i] = class_name
       # average damage
       newdf['damage_percentage'][i] = (sum(df2.overall_damage) / len(df2.overall_damage))
   # print(newdf.to_string())
   data = [newdf.damage_percentage.to_list()]
   final_df = DataFrame(data, columns=newdf.part_name.to_list())

   ''' final_output_format '''
   final_df.insert(0, 'case_id', case)

   '''covert to dict format to dump into database'''
   mydict = final_df.to_dict('records')
   # print(mydict[0])

   '''insert data into the database'''
   collection.insert_one(mydict[0])
   end=time.time()
print(end-start)