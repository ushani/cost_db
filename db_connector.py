from sshtunnel import SSHTunnelForwarder


MONGO_HOST = "34.245.17.199"
MONGO_USER = "ubuntu"
MONGO_PKEY="./id_rsa"

def connect_to_db():
   server = SSHTunnelForwarder(
    MONGO_HOST,
    ssh_username=MONGO_USER,
    ssh_pkey=MONGO_PKEY,
    remote_bind_address=('127.0.0.1', 27017)
      )
   return server




